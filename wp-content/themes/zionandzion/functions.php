<?php
class Theme {
  function __construct() {
    require_once get_template_directory() . '/classes/Admin.php';
    require_once get_template_directory() . '/classes/Cache.php';
    require_once get_template_directory() . '/classes/Media.php';
    require_once get_template_directory() . '/classes/Rest.php';
    require_once get_template_directory() . '/zz-editor-buttons/zz-btns.php';

    new Admin;
    new Cache;
    new Media;
    new Rest;
  }
}
new Theme;
