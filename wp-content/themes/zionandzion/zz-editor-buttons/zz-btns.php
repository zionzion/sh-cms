<?php
add_action('init', 'zz_add_my_tc_button');

function zz_add_my_tc_button() {
    if (get_current_blog_id() === 4) {
        global $typenow;

        // Check user permissions
        if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
            return;
        }
        // Verify the post type - turn on if you want to only show these custom buttons in some area of the site
        //  if( !in_array( $typenow, array( 'post', 'page' ) ) )
        //    return;

        // Check if WYSIWYG is enabled
        if ( get_user_option('rich_editing') == 'true') {
            add_filter('mce_external_plugins', 'zz_add_tinymce_plugin');
            add_filter('mce_buttons', 'zz_register_my_tc_button');
        }

        wp_register_style( 'zz_tc_button_css', get_template_directory_uri() . '/zz-editor-buttons/zz-btns.css', false, '1.0.0' );
        wp_enqueue_style( 'zz_tc_button_css' );
    }
}

function zz_add_tinymce_plugin($plugin_array) {
    if (get_current_blog_id() === 4) {
        $plugin_array['zz_tc_button'] = get_template_directory_uri() . '/zz-editor-buttons/zz-btns.js';
        $plugin_array['zz_tc_list'] = get_template_directory_uri() . '/zz-editor-buttons/zz-btns.js';
        return $plugin_array;
    }
}

function zz_register_my_tc_button($buttons) {
    if (get_current_blog_id() === 4) {
        array_push($buttons, 'zz_tc_button');
        array_push($buttons, 'zz_tc_list');
        return $buttons;
    }
}
