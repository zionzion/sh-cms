tinymce.PluginManager.add('zz_tc_button', function( editor, url ) {
  let svg_url = window.location.protocol + '//' + window.location.hostname + '/wordpress/wp-content/themes/zionandzion/zz-editor-buttons/flower-detail.svg'

  editor.addButton( 'zz_tc_button', {
    title: 'Insert Button',
    icon: 'icon zz-mce-button',
    onclick: function() {
      editor.windowManager.open( {
        title: 'Insert Button',
        width: 500,
        height: 300,
        body: [
        {
          type: 'textbox',
          name: 'url',
          label: 'URL'
        },
        {
          type: 'textbox',
          name: 'label',
          label: 'Link Text'
        },
        {
          type: 'checkbox',
          name: 'newtab',
          label: ' ',
          text: 'Open link in new tab',
          checked: true
        },
        {
          type: 'checkbox',
          name: 'decorations',
          label: ' ',
          text: 'Add decorations to button',
          checked: false
        },
        {
          type: 'listbox',
          name: 'style',
          label: 'Button Style',
          'values': [
            {text: 'Light Blue', value: 'lt-blue'},
            {text: 'Dark Blue', value: 'dk-blue'},
            {text: 'Dark Blue', value: 'dk-blue'},
          ]
        }],
        onsubmit: function( e ) {
          let $content = (!!e.data.decorations ? '<div class="button decoration-container"><img src="' + svg_url + '" alt="Decorative Detail" class="before-btn">' : '') + '<a href="' + e.data.url + '" class="v-btn ' + e.data.style + '"' + (!!e.data.newtab ? ' target="_blank"' : '' ) + '><span class="v-btn__content">' + e.data.label + '</span><span class="btn-arrow"></span><span class="btn-overlay"></span></a>' + (!!e.data.decorations ? '<img src="' + svg_url + '" alt="Decorative Detail" class="after-btn"></div>' : '');
          editor.insertContent( $content );
        }
      });
    }
  });
});
tinymce.PluginManager.add('zz_tc_list', function( editor, url ) {
  editor.addButton( 'zz_tc_list', {
    title: 'Insert List',
    icon: 'icon zz-mce-list',
    onclick: function() {
      editor.windowManager.open( {
        title: 'Insert List',
        width: 500,
        height: 300,
        body: [
        {
          type: 'container',
          label: 'Instructions',
          html: '&nbsp;<br /><i>Choose the type of list and the number of<br /> columns that will display on desktop.<br /></i>&nbsp;'
        },
        {
          type: 'listbox',
          name: 'type',
          label: 'List Type',
          tooltip: 'Choose whether this is an unordered bulleted list or a numbered list.',
          'values': [
            {text: 'Bulleted List', value: 'ul'},
            {text: 'Numbered List', value: 'ol'},
          ]
        },
        {
          type: 'listbox',
          name: 'cols',
          label: 'Number of Columns',
          tooltip: 'Choose the number of columns that will display on the website on desktop.',
          'values': [
            {text: '1', value: '1'},
            {text: '2', value: '2'},
            {text: '3', value: '3'},
            {text: '4', value: '4'},
          ]
        }],
        onsubmit: function( e ) {
          let i,
              $lipsum = '';
          for (i = 0; i < e.data.cols; i++) {
            $lipsum += '<li>Lorem ipsum dolor sit amet</li>'
          }

          let $content = '<' + e.data.type + ' class="cols-' + e.data.cols + '">' + $lipsum + '</' + e.data.type + '>';
          editor.insertContent( $content );
        }
      });
    }
  });
});