<?php
$today = new DateTime(date('F j, Y'));

switch_to_blog(2);

$args = array(
    'post_type'       => 'event',
    'posts_per_page'  => -1
);

$posts = get_posts($args);

foreach($posts as $post){
    $id = $post->ID;
    $post_date = new DateTime(get_field('event_date', $id));

    if($post_date < $today){
        $data = array(
            'ID' => $id,
            'post_status' => 'draft'
        );

        wp_update_post($data);
    }
}