<?php
require_once get_template_directory() . '/classes/Base.php';

class Rest extends Base {
  function __construct() {
    add_action('init', array($this, 'initHook') );
    add_action('rest_api_init', array($this, 'restApiInitHook') );
    add_filter('rest_post_dispatch', array($this, 'restPostDispatchHook'), 10, 3);
    add_filter('rest_prepare_revision', array($this, 'add_to_revision_rest_api'), 10, 2 );
  }

  private function formatCarouselSlides (&$component) {
    $whitelist = array('carousel', 'thumbnail-carousel');
    if ( in_array($component['acf_fc_layout'], $whitelist) ){
      foreach( $component['desktop_slides'] as &$slide ) {
        if ( get_field('carousel_link', $slide['id']) ) {
          $slide['carousel_link'] = get_field('carousel_link', $slide['id']);
        }
      }
      if ($component['mobile_slides']){
        foreach( $component['mobile_slides'] as &$slide ) {
          if ( get_field('carousel_link', $slide['id']) ) {
            $slide['carousel_link'] = get_field('carousel_link', $slide['id']);
          }
        }
      }
    }
  }

  public function restApiInitHook () {
    $this->setAccess();
  }

  public function initHook () {
    $this->setAccess();
  }

  public function setAccess () {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
  }

  function rest_api_add_featured_image(){
    register_rest_field( array('post'),
        'fimg_url',
        array(
            'get_callback'    => 'get_rest_featured_image',
            'update_callback' => null,
            'schema'          => null,
        )
    );
  }

  function get_rest_featured_image( $object, $field_name, $request ) {
    if( $object['featured_media'] ){
      $img = wp_get_attachment_image_src( $object['featured_media'], 'app-thumb' );
      return $img[0];
    }
    return false;
  }

  public function restPostDispatchHook ($result, $wpThis, $request ) {


    // NAVIGATION MENUS ONLY (NEEDS TRAILING SLASH)
    if( strpos($_SERVER['REQUEST_URI'], '/wp-api-menus/v2/menus/') !== false) {
      foreach ( $result->data['items'] as $k => &$item ) {
        $result->data['items'][$k]['display_type'] = (get_field('display_type', $item['id'])) ? get_field('display_type', $item['id']) : '';
        $result->data['items'][$k]['subtitle'] = (get_field('subtitle', $item['id'])) ? get_field('subtitle', $item['id']) : '';
        $result->data['items'][$k]['thumbnail'] = (get_field('thumbnail', $item['id'])) ? get_field('thumbnail', $item['id']) : '';
        $result->data['items'][$k]['link_label'] = (get_field('link_label', $item['id'])) ? get_field('link_label', $item['id']) : '';
        $result->data['items'][$k]['cta_description'] = (get_field('cta_description', $item['id'])) ? get_field('cta_description', $item['id']) : '';
        $result->data['items'][$k]['featured_classes'] = (get_field('featured_classes', $item['id'])) ? get_field('featured_classes', $item['id']) : '';

        if (isset($item['children'])) {
          foreach ( $item['children'] as $c_k => &$c_item ) {
            $result->data['items'][$k]['children'][$c_k]['display_type'] = (get_field('display_type', $c_item['id'])) ? get_field('display_type', $c_item['id']) : '';
            $result->data['items'][$k]['children'][$c_k]['subtitle'] = (get_field('subtitle', $c_item['id'])) ? get_field('subtitle', $c_item['id']) : '';
            $result->data['items'][$k]['children'][$c_k]['thumbnail'] = (get_field('thumbnail', $c_item['id'])) ? get_field('thumbnail', $c_item['id']) : '';
            $result->data['items'][$k]['children'][$c_k]['link_label'] = (get_field('link_label', $c_item['id'])) ? get_field('link_label', $c_item['id']) : '';
            $result->data['items'][$k]['children'][$c_k]['cta_description'] = (get_field('cta_description', $c_item['id'])) ? get_field('cta_description', $c_item['id']) : '';
            $result->data['items'][$k]['children'][$c_k]['featured_classes'] = (get_field('featured_classes', $c_item['id'])) ? get_field('featured_classes', $c_item['id']) : '';
          }
        }
      }
    }

    // PAGES OR POSTS WITH SECTIONS ONLY
    if ( isset($result->data[0]['acf']['sections']) && is_array($result->data[0]['acf']['sections']) ) {
      foreach ( $result->data[0]['acf']['sections'] as &$section ) {
        if (isset($section['components'])) {
          foreach ( $section['components'] as &$component ) {
            $this->formatCarouselSlides($component);
          }
        } else if (isset($section['left_components'])) {
          foreach ( $section['left_components'] as &$component ) {
            $this->formatCarouselSlides($component);
          }
        } else if (isset($section['center_components'])) {
          foreach ( $section['center_components'] as &$component ) {
            $this->formatCarouselSlides($component);
          }
        } else if (isset($section['right_components'])) {
          foreach ( $section['right_components'] as &$component ) {
            $this->formatCarouselSlides($component);
          }
        }
      }
    }

    return $result;
  }

  // Adds ACF fields to revision Rest API
  function add_to_revision_rest_api( $response, $post ) {
    $data = $response->get_data();
    $data['acf'] = get_fields( $post->ID );

    return rest_ensure_response( $data );
  }
}
?>