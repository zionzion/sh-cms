<?php
require_once get_template_directory() . '/classes/Base.php';

class Cache extends Base {
  function __construct() {
    add_action('wp_update_nav_menu', array($this, 'clearCacheMenu'));
    add_action('save_post', array($this, 'clearCachePost'), 10, 3);
    add_action('acf/save_post', array($this, 'clearCacheOptions'), 20);
  }

  public function clearCachePost( $id, $data, $updated ) {
    $post = get_post($id);

    if ($updated === true && $post->post_type !== 'nav_menu_item') {
      echo '<pre>';
      print_r($post->post_title);
      echo '</pre>';

      foreach ($this->getSiteUrls() as $url) {
          $ch = curl_init($url . '/zz/cache/clear?path=all');
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $response = curl_exec($ch);
          curl_close($ch);
      }
    }
  }

  public function clearCacheMenu ($id, $datq = null) {
    $menu = wp_get_nav_menu_object($id);

    foreach ($this->getSiteUrls() as $url) {
      $ch = curl_init($url . '/zz/cache/clear?path=all');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($ch);
      curl_close($ch);
    }
  }

  public function clearCacheOptions($id) {
    if ($id === 'site-information') {
      foreach ($this->getSiteUrls() as $url) {
        $ch = curl_init($url.'/zz/cache/clear?path=site');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
      }
    }
    if ($id === 'network-information') {
      foreach ($this->getSiteUrls() as $url) {
        $ch = curl_init($url.'/zz/cache/clear?path=network');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
      }
    }
  }
}
?>
