<?php
require_once get_template_directory() . '/classes/Base.php';

class Media extends Base {
  function __construct() {
    add_image_size( 'xs', 600, 600 );
    add_image_size( 'sm', 960, 960 );
    add_image_size( 'md', 1264, 1264 );
    add_image_size( 'lg', 1904, 1904 );
    add_image_size( 'xl', 2200, 2200 );

    add_filter( 'image_size_names_choose', array($this, 'customMediaSizes') );
  }

  public function customMediaSizes ($sizes) {
    return array_merge( $sizes, array(
      'xs' => __( 'Extra Small' ),
      'sm' => __( 'Small' ),
      'md' => __( 'Medium' ),
      'lg' => __( 'Large' ),
      'xl' => __( 'Extra Large' )
    ) );
  }

}
?>