<?php
abstract class Base {
  protected $urls = array(
    // OFFSET
    array(),
    // FOUNDATION
    array(
      'production' => 'https://www.sunhealth.org',
      'staging' => 'https://staging.sunhealthfoundation.org',
    ),
    // COMMUNITIES
    array(
      'production' => 'https://www.sunhealthcommunities.org',
      'staging' => 'https://staging.sunhealthcommunities.org',
    ),
    // WELLNESS
    array(
      'production' => 'https://www.sunhealthwellness.org',
      'staging' => 'https://staging.sunhealthwellness.org'
    ),
    // CORPORATE
    array(
//      'production' => 'https://www.sunhealth.org',
      'staging' => 'https://staging.sunhealth.org'
    )
  );

  function __construct() {

  }

  protected function getSiteUrls () {
    $siteId = get_current_blog_id();
    return $this->urls[$siteId];
  }

  protected function logThis( $message ) {
    if( WP_DEBUG === true ){
      if( is_array( $message ) || is_object( $message ) ){
        error_log( print_r( $message, true ) );
      } else {
        error_log( $message );
      }
    }
  }
}
?>