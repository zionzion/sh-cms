<?php
require_once get_template_directory() . '/classes/Base.php';

class Admin extends Base
{
    function __construct()
    {
        add_theme_support('menus');
        add_theme_support('post-thumbnails');

        add_action('acf/init', array($this, 'my_acf_init'));
        add_action('init', array($this, 'change_post_object_label'));
        add_action('admin_menu', array($this, 'change_post_menu_label'));
        add_action('admin_menu', array($this, 'ccd_menu_news_icon'));
        add_action('admin_menu', array($this, 'removeItemsFromAdmin'));
        add_action('wpseo_metabox_prio', array($this, 'yoast_to_bottom'));
//
        add_filter('init', array($this, 'virtual_page_update_events'));
        add_filter('admin_footer_text', array($this, 'remove_footer_admin'));
        add_filter('ACFFA_get_icons', array($this, 'get_icons'), 10, 1);
        add_filter('preview_post_link', array($this, 'custom_preview_page_link'));
        add_filter('gform_pre_validation', array($this, 'custom_gform_pre_validation'));
        add_action('acf/save_post',      array($this, 'save_video_thumbnail'), 20);
        add_shortcode('emphasis', array($this, 'emphasis_shortcode'));
        add_shortcode('progress_bar', array($this, 'progress_bar_shortcode'));
        add_action('wp_dashboard_setup', array($this, 'shortcode_dashboard_widget'));
        add_action('init', array($this, 'custom_corporate_posts'), 0);
        add_filter('tiny_mce_before_init', array($this, 'override_mce_options'));
        add_action('post_submitbox_misc_actions', array($this, 'fix_autosave'));

        add_action('rest_api_init', function() {
          register_rest_route('/wp/v2', '/(?P<post_type>[a-zA-Z0-9-]+)/last_edit/(?P<slug>[a-zA-Z0-9-]+)', [
            'methods' => 'GET',
            'callback' => array($this, 'modified_date'),
          ]);
        });

        // ACF Options pages
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page(array(
                'page_title' => 'Site Information',
                'menu_title' => 'Site Information',
                'menu_slug' => 'site-information',
                'capability' => 'edit_posts',
                'position' => false,
                'parent_slug' => '',
                'icon_url' => 'dashicons-admin-site',
                'redirect' => true,
                'post_id' => 'site-information',
                'autoload' => false,
                'update_button' => __('Save', 'acf'),
                'updated_message' => __("Site Information Updated", 'acf'),
            ));

            if (get_current_blog_id() == 1) {
                acf_add_options_page(array(
                    'page_title' => 'Network Information',
                    'menu_title' => 'Network Information',
                    'menu_slug' => 'network-information',
                    'capability' => 'edit_posts',
                    'position' => false,
                    'parent_slug' => '',
                    'icon_url' => 'dashicons-networking',
                    'redirect' => true,
                    'post_id' => 'network-information',
                    'autoload' => false,
                    'update_button' => __('Save', 'acf'),
                    'updated_message' => __("Network Information Updated", 'acf'),
                ));
            }
        };
    }

    function modified_date( $data ) {

      $bid = get_current_blog_id();
      switch_to_blog($bid);

      $args=array(
        'name'           => $data['slug'],
        'post_type'      => $data['post_type'],
        'post_status'    => 'publish',
        'posts_per_page' => 1
      );

      $posts = get_posts( $args );

      if ( empty( $posts ) ) {
        return null;
      }

      return $posts[0]->post_modified;
    }

    // Create meta button to remove disabled class from Publish, Save Draft and Preview buttons
    function fix_autosave() { ?>
        <a href="javascript:;"
           style="float:right;margin:10px;"
           onclick="jQuery('#submitpost input').removeClass('disabled');return false;">Unlock</a>
        <?php
    }

    // Google API Key
    function my_acf_init() {
        acf_update_setting('google_api_key', 'AIzaSyBd4LE47E1EoNYoaDObtXBj7t0Hz3scRhI');
    }

    // Add Custom Post Types
    function change_post_object_label()
    {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        if (get_current_blog_id() == 2) {
            $labels->name = 'Helpful Tools';
            $labels->singular_name = 'Helpful Tool';
            $labels->add_new = 'Add Helpful Tool';
            $labels->add_new_item = 'Add Helpful Tool';
            $labels->edit_item = 'Edit Helpful Tool';
            $labels->new_item = 'Helpful Tool';
            $labels->view_item = 'View Helpful Tool';
            $labels->search_items = 'Search Helpful Tools';
            $labels->not_found = 'No Helpful Tools found';
            $labels->not_found_in_trash = 'No Helpful Tool found in Trash';
            $labels->name_admin_bar = 'Add Helpful Tool';
        } else if (get_current_blog_id() == 1) {
            $labels->name = 'Articles';
            $labels->singular_name = 'Articles';
            $labels->add_new = 'Add Article';
            $labels->add_new_item = 'Add Article';
            $labels->edit_item = 'Edit Article';
            $labels->new_item = 'Article';
            $labels->view_item = 'View Article';
            $labels->search_items = 'Search Articles';
            $labels->not_found = 'No Articles found';
            $labels->not_found_in_trash = 'No Articles found in Trash';
            $labels->name_admin_bar = 'Add Article';
        } else {
            $labels->name = 'News & Events';
            $labels->singular_name = 'News & Events';
            $labels->add_new = 'Add New & Event';
            $labels->add_new_item = 'Add New & Event';
            $labels->edit_item = 'Edit New & Event';
            $labels->new_item = 'New & Event';
            $labels->view_item = 'View New & Event';
            $labels->search_items = 'Search News & Events';
            $labels->not_found = 'No News & Events found';
            $labels->not_found_in_trash = 'No News & Events found in Trash';
            $labels->name_admin_bar = 'Add New & Event';
        }
    }

    // Change WP admin label of posts
    function change_post_menu_label()
    {
        global $menu;
        global $submenu;
        if (get_current_blog_id() == 2) {
            $menu[5][0] = 'Helpful Tools';
            $submenu['edit.php'][5][0] = 'Helpful Tools';
            $submenu['edit.php'][10][0] = 'Add Helpful Tool';
        } else if (get_current_blog_id() == 1) {
            $menu[5][0] = 'Articles';
            $submenu['edit.php'][5][0] = 'Articles';
            $submenu['edit.php'][10][0] = 'Add Article';
        } else {
            $menu[5][0] = 'News & Events';
            $submenu['edit.php'][5][0] = 'News & Events';
            $submenu['edit.php'][10][0] = 'Add New & Event';
        }
        echo '';
    }

  // Change WP admin icon on Articles
  function ccd_menu_news_icon()
  {
    global $menu;
    foreach ($menu as $key => $val) {
      if (__('Articles') == $val[0] || __('Resource Center') == $val[0] || __('News & Events') == $val[0]) {
        $menu[$key][6] = 'dashicons-megaphone';
      }
    }
  }

  // Remove Comments from WP admin sidebar
  function removeItemsFromAdmin()
  {
    remove_menu_page('edit-comments.php');
  }

  // Move Yoast to bottom of admin pages
  function yoast_to_bottom()
  {
    return 'low';
  }


  // Virtual Page for Update Events
  function virtual_page_update_events($template)
  {
    if (isset($_GET['update_events'])) {
      include get_template_directory() . '/update_events.php';
      die;
    }
  }

  // Change WP admin footer text
  function remove_footer_admin()
  {
    echo 'Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Developed by: <a href="https://www.zionandzion.com" target="_blank">Zion&Zion</a></p>';
  }

  // ACF Font Awesome Icon List Filter
  function get_icons($results)
  {
    $results = array(
      "list" => array(
        "fab" => array(
          "fab fa-facebook-f" => "<i class='fab'>&#xf39e;</i> facebook-f",
          "fab fa-twitter" => "<i class='fab'>&#xf099;</i> twitter",
          "fab fa-linkedin-in" => "<i class='fab'>&#xf0e1;</i> linkedin-in",
          "fab fa-youtube" => "<i class='fab'>&#xf167;</i> youtube"
        ),
        "fas" => array(
          "fas fa-apple-alt" => "<i class='fas'>&#xf5d1;</i> apple-alt",
          "fas fa-atom" => "<i class='fas'>&#xf5d2;</i> atom",
          "fas fa-band-aid" => "<i class='fas'>&#xf462;</i> band-aid",
          "fas fa-bone" => "<i class='fas'>&#xf5d7;</i> bone",
          "fas fa-book-open" => "<i class='fas'>&#xf518;</i> book-open",
          "fas fa-brain" => "<i class='fas'>&#xf5dc;</i> brain",
          "fas fa-chalkboard-teacher" => "<i class='fas'>&#xf51c;</i> chalkboard-teacher",
          "fas fa-desktop" => "<i class='fas'>&#xf108;</i> desktop",
          "fas fa-file-medical-alt" => "<i class='fas'>&#xf478;</i> file-medical-alt",
          "fas fa-h-square" => "<i class='fas'>&#xf0fd;</i> h-square",
          "fas fa-hand-holding-heart" => "<i class='fas'>&#xf4be;</i> hand-holding-heart",
          "fas fa-hands" => "<i class='fas'>&#xf4c2;</i> hands",
          "fas fa-hands-helping" => "<i class='fas'>&#xf4c4;</i> hands-helping",
          "fas fa-heart" => "<i class='fas'>&#xf004;</i> heart",
          "fas fa-heartbeat" => "<i class='fas'>&#xf21e;</i> heartbeat",
          "fas fa-hospital" => "<i class='fas'>&#xf0f8;</i> hospital",
          "fas fa-laptop" => "<i class='fas'>&#xf109;</i> laptop",
          "fas fa-medkit" => "<i class='fas'>&#xf0fa;</i> medkit",
          "fas fa-microscope" => "<i class='fas'>&#xf610;</i> microscope",
          "fas fa-mortar-pestle" => "<i class='fas'>&#xf5a7;</i> mortar-pestle",
          "fas fa-notes-medical" => "<i class='fas'>&#xf481;</i> notes-medical",
          "fas fa-prescription" => "<i class='fas'>&#xf5b1;</i> prescription",
          "fas fa-prescription-bottle" => "<i class='fas'>&#xf485;</i> prescription-bottle",
          "fas fa-procedures" => "<i class='fas'>&#xf487;</i> procedures",
          "fas fa-ribbon" => "<i class='fas'>&#xf4d6;</i> ribbon",
          "fas fa-seedling" => "<i class='fas'>&#xf4d8;</i> seedling",
          "fas fa-star-of-life" => "<i class='fas'>&#xf621;</i> star-of-life",
          "fas fa-stethoscope" => "<i class='fas'>&#xf0f1;</i> stethoscope",
          "fas fa-user-md" => "<i class='fas'>&#xf0f0;</i> user-md",
          "fas fa-users" => "<i class='fas'>&#xf0c0;</i> users"
        ),
        "far" => array(
          "far fa-heart" => "<i class='far'>&#xf004;</i> heart"
        )
      ),
      "details" => array(
        "fab" => array(
          "fab fa-facebook-f" => array("hex" => "\f39e", "unicode" => "&#xf39e;"),
          "fab fa-twitter" => array("hex" => "\f099", "unicode" => "&#xf099;"),
          "fab fa-linkedin-in" => array("hex" => "\f0e1", "unicode" => "&#xf0e1;"),
          "fab fa-youtube" => array("hex" => "\f431", "unicode" => "&#xf431;")
        ),
        "fas" => array(
          "fas fa-apple-alt" => array("hex" => "\f5d1", "unicode" => "&#xf5d1;"),
          "fas fa-atom" => array("hex" => "\f5d2", "unicode" => "&#xf5d2;"),
          "fas fa-band-aid" => array("hex" => "\f462", "unicode" => "&#xf462;"),
          "fas fa-bone" => array("hex" => "\f5d7", "unicode" => "&#xf5d7;"),
          "fas fa-book-open" => array("hex" => "\f518", "unicode" => "&#xf518;"),
          "fas fa-brain" => array("hex" => "\f5dc", "unicode" => "&#xf5dc;"),
          "fas fa-chalkboard-teacher" => array("hex" => "\f51c", "unicode" => "&#xf51c;"),
          "fas fa-desktop" => array("hex" => "\f108", "unicode" => "&#xf108;"),
          "fas fa-file-medical-alt" => array("hex" => "\f478", "unicode" => "&#xf478;"),
          "fas fa-h-square" => array("hex" => "\ff0f", "unicode" => "&#xff0f;"),
          "fas fa-hand-holding-heart" => array("hex" => "\f4be", "unicode" => "&#xf4be;"),
          "fas fa-hands" => array("hex" => "\f4c2", "unicode" => "&#xf4c2;"),
          "fas fa-hands-helping" => array("hex" => "\f4c4", "unicode" => "&#xf4c4;"),
          "fas fa-heart" => array("hex" => "\f004", "unicode" => "&#xf004;"),
          "fas fa-heartbeat" => array("hex" => "\f21e", "unicode" => "&#xf21e;"),
          "fas fa-hospital" => array("hex" => "\f0f8", "unicode" => "&#xf0f8;"),
          "fas fa-laptop" => array("hex" => "\f109", "unicode" => "&#xf109;"),
          "fas fa-medkit" => array("hex" => "\f0fa", "unicode" => "&#xf0fa;"),
          "fas fa-mortar-pestle" => array("hex" => "\f5a7", "unicode" => "&#xf5a7;"),
          "fas fa-notes-medical" => array("hex" => "\f481", "unicode" => "&#xf481;"),
          "fas fa-prescription" => array("hex" => "\f5b1", "unicode" => "&#xf5b1;"),
          "fas fa-prescription-bottle" => array("hex" => "\f485", "unicode" => "&#xf485;"),
          "fas fa-procedures" => array("hex" => "\f487", "unicode" => "&#xf487;"),
          "fas fa-ribbon" => array("hex" => "\f4d6", "unicode" => "&#xf4d6;"),
          "fas fa-seedling" => array("hex" => "\f4d8", "unicode" => "&#xf4d8;"),
          "fas fa-star-of-life" => array("hex" => "\f621", "unicode" => "&#xf621;"),
          "fas fa-stethoscope" => array("hex" => "\f0f1", "unicode" => "&#xf0f1;"),
          "fas fa-user-md" => array("hex" => "\f0f0", "unicode" => "&#xf0f0;"),
          "fas fa-users" => array("hex" => "\f0c0", "unicode" => "&#xf0c0;"),
        ),
        "far" => array(
          "far fa-heart" => array("hex" => "\f004", "unicode" => "&#xf004;")
        )
      )
    );
    return $results;
  }

  // Update Preview button link
  function custom_preview_page_link()
  {
    $id = get_the_ID();
    $revs = wp_get_post_revisions(get_post($id));
    $slug = str_replace(home_url(), '', get_permalink());
    $categories = wp_get_post_categories($id);
    $cats = array();
    $post_type = get_post_type();

    if (!empty($revs)) {
      $latest_rev = max($revs)->ID;

      // Gets production URL if exists, fallback to dev
      $urlArray = $this->getSiteUrls();
      $urlArray = array_values($urlArray);
      if (count($urlArray) > 1) {
        $url = $urlArray[0];
      } else {
        $url = 'http://localhost:3000';
      }

      // Check for difficult slugs and adjust accordingly
      if (get_current_blog_id() === 1) { //Foundation
        if($post_type === 'post'){
          $slug = '/articles/foundation' . $slug;
        }

        // CPT - Current Campaign
        if (strpos($slug, '/campaign/') !== false) {
          $slug = str_replace("/campaign", '/current-campaign', $slug);
        }

        // CPT - Impact Story
        if (strpos($slug, '/impact-story/') !== false) {
          $slug = str_replace("/impact-story", '/impact-stories', $slug);
        }
      } elseif (get_current_blog_id() === 2) { //Communities
        // Resource posts
        if (!empty($categories)) {
          foreach ($categories as $c) {
            $cat = get_category($c);
            $cats[] = array('slug' => $cat->slug);
          }
          $slug = '/helpful-tools/' . $cats[0]['slug'] . $slug;
        }

        // CPT - At Home
        if (strpos($slug, '/at-home/') !== false) {
          $slug = str_replace("/at-home", '', $slug);
        }
      } else { //Wellness
        if (!empty($categories)) {
          foreach ($categories as $c) {
            $cat = get_category($c);
            $cats[] = array('slug' => $cat->slug);
          }
          $slug = '/' . $cats[0]['slug'] . $slug;
        }
      }

      if($slug === '/careers/') { $slug = '/careers'; }

      $link = $url . $slug . '?page_id=' . $id . '&preview_id=' . $latest_rev;

      return $link;
    }

    return false;
  }

  // Gravity Forms - Pre-validation add state to post data
  function custom_gform_pre_validation($form){
    $name = 'state_' . absint( $form['id'] );
    if ( ! isset( $_POST[ $name ] ) ) {
      $field_values   = rgpost( 'gform_field_values' );
      $_POST[ $name ] = GFFormDisplay::get_state( $form, $field_values );
    }

    return $form;
  }

  function save_video_thumbnail($post_id){
    if (get_current_blog_id() === 3) {
      if (get_post_type($post_id) !== 'post') {
        return;
      }

      $video_url = get_field('video');

      if($video_url !== ''){
        preg_match('/src="([^"]+)"/', $video_url, $match);
        $src = $match[1];
        $attr = explode('/', $src)[4];
        $id = explode('?', $attr)[0];
        $thumbnail = 'https://img.youtube.com/vi/' . $id . '/0.jpg';
        update_field('video_thumbnail', $thumbnail);
      }
    }
  }

  function emphasis_shortcode( $atts, $content ){
    return "<div class='emphasized-content'>" . $content . "</div>";
  }

  function progress_bar_shortcode( $atts, $content ){
    $atts = shortcode_atts(
      array(
        'percent' => 100
      ), $atts, 'progress_bar');

    return "<div class='progress-bar-container'>
                        <div class='bar-container'>
                            <div class='progress-bar' style='width:" . $atts['percent'] . "%;'>
                                <div class='progress-number'>" . + $atts['percent'] . "%</div>
                                <span class='chevron'></span>
                            </div>
                        </div>
                    </div>";
  }

  function shortcode_dashboard_widget() {
    if (get_current_blog_id() === 3) {
      wp_add_dashboard_widget('zz_custom_shortcodes', 'Custom Shortcode Reference', function () {
        echo "<p>Most shortcodes can be used by surrounding text with the opening and closing tag, e.g.:\n";
        echo "[<i>tagname</i>]Some content[/<i>tagname</i>]</p>";
        echo "<dl>\n<dt>[emphasis]</dt>\n<dd>Will display list items side by side in a bold font. Best used for pricing and services.</dd>\n</dl>";
      });
    }
  }

    // Add Team Members to Corporate Website since Pods is not working
    // ToDo: Update Wordpress Core to fix issue with Pods Plugin
    function custom_corporate_posts() {
        if (get_current_blog_id() === 4) {
            register_post_type( 'team_member',
                // CPT Options
                array(
                    'labels' => array(
                        'name' => __( 'Team Members' ),
                        'singular_name' => __( 'Team Member' )
                    ),
                    'show_ui' => true,
                    'show_in_admin_bar' => true,
                    'menu_position' => 22,
                    'menu_icon' => 'dashicons-groups',
                    'show_in_menu' => true,
                    'show_in_nav_menus' => true,
                    'public' => true,
                    'publicly_queryable' => true,
                    'supports' => array('title', 'editor', 'revisions'),
                    'show_in_rest' => true
                )
            );
        }
    }

    // Allow spans within wysiwyg for button editor
    function override_mce_options($initArray) {
        $opts = '*[*]';
        $initArray['valid_elements'] = $opts;
        $initArray['extended_valid_elements'] = $opts;
        return $initArray;
    }
}
