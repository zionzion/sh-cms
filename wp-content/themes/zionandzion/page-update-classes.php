<?php
$current = date('m Y');

switch_to_blog(3);

$args = array(
    'post_type'       => 'class',
    'posts_per_page'  => -1
);

$posts = get_posts($args);

foreach($posts as $post){

    $id = $post->ID;
    $class_month = date('m Y', strtotime(get_field('class_date', $id)));

    if($class_month < $current){
        $data = array(
            'ID' => $id,
            'post_status' => 'draft'
        );

        wp_update_post($data);
    }
}