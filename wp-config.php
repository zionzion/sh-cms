<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'shcms_wp' );

/** MySQL database username */
define( 'DB_USER', 'shcms_wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'XzPpZu2hYlnl' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-|+-4?383F$ |F]YiAb6LU}a+,RtG/KN*;A&d[?_n*HcwD-aOC#SpauO|n5mPenb');
define('SECURE_AUTH_KEY',  '_5U-JKzo}6Y5#7!Ukm;:Bxtt_ir@$9Dgk_MG3Ao5B 8xYX+h{;G(. sS2&&!?B.6');
define('LOGGED_IN_KEY',    '4O7:[NBW Q.W{ZizaT6JY(dK5+c(dVk>rUux#WbHpjB*oHQBbVlb Vxv8)<F<#xM');
define('NONCE_KEY',        'bY%>knc8qk#aQ3uyWaRu]o72+7CQWd{AL^+hTzeX&LmHT:&zpuhx{m19r+BDGKQ ');
define('AUTH_SALT',        '/rPaAYW[.w|h d1Y?dV74fK_cA_z>)(0oU5wcOp9GN@!b0tb&{K;;7t{t-|-&`YJ');
define('SECURE_AUTH_SALT', 'XIdEr=m8)g0uY3%Pj7-kr%pF1XdEje* 3lrfBccrZkoX2jiZafW;naO2&R&XZJVH');
define('LOGGED_IN_SALT',   'Iu7J} */~crK7#OxiI-`I0mY&VRd3LMPB*0.^<@[jK@=:qS_(D ZPYKCOvOC{E$}');
define('NONCE_SALT',       '7n:bG7__:k7K[FSY$U2Lv!^cey|.=7JcORcC8$$^Egu0/7rqjo-|WMX)s,VuAjV ');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * WordPress Debugging
 *
 * Set to "true" to enable wp_debug. Set to "false" to disable.
 * Do not include quotes when setting to "true" or "false".
 */
define( 'WP_DEBUG', false );

define('AUTOSAVE_INTERVAL', 86400);

/* Multisite */
define('WP_ALLOW_MULTISITE', true);

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'cms.sunhealth.org');
define('PATH_CURRENT_SITE', '/wordpress/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Adds support for JWT Authentication for WP Rest API */
define('JWT_AUTH_SECRET_KEY', '-|+-4?383F$ |F]YiAb6LU}a+,RtG/KN*;A&d[?_n*HcwD-aOC#SpauO|n5mPenb');
define('JWT_AUTH_CORS_ENABLE', true);
define('JWT_AUTH_EXPIRE', time()+((60*60*24*7*30*365) * 10)); // YEAR IN SECONDS NOT BEING RECOGNIZED SO TOOK THEIR CALCULATION OF VALUE TO REPLACE CONSTANT

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
